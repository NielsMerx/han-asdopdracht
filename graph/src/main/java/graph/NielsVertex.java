package graph;
import datatypes.*;

public class NielsVertex implements Comparable<NielsVertex>
{
    private String name;
    public HANLinkedList<NielsEdge> edges;

    public NielsVertex(String name)
    {
        this.name = name;
        this.edges = new HANLinkedList<NielsEdge>();
    }

    public void add(NielsEdge edge, int cost)
    {
        edge.setCost(cost);
        this.edges.add(edge);

    }

    public String getName()
    {
        return this.name;
    }

    public boolean isConnected()
    {
        if(this.edges.getSize() >0)
            return true;

        return false;
    }


    private int getLowestCostEdge()
    {
        int lowest = Integer.MAX_VALUE;
        for(int i = 0; i< this.edges.getSize(); i++)
        {
            if(this.edges.get(i).getCost() <lowest)
                lowest = this.edges.get(i).getCost();
        }

        return lowest;
    }


    @Override
    public int compareTo(NielsVertex o) {
        if(this.getLowestCostEdge() == o.getLowestCostEdge())
            return 0;
        else if(this.getLowestCostEdge() > o.getLowestCostEdge())
            return 1;
        else
            return -1;
    }
}


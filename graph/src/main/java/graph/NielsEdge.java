package graph;

public class NielsEdge implements Comparable<NielsEdge>
{
    private int cost;
    private NielsVertex vertex;

    public NielsEdge(NielsVertex vertex)
    {
        this.vertex = vertex;
    }

    public NielsVertex getVertex()
    {
        return this.vertex;
    }

    public int getCost() {
        return cost;
    }
    public void setCost(int cost)
    {
        this.cost =cost;
    }


    @Override
    public int compareTo(NielsEdge o) {
        if(cost==o.cost)
            return 0;
        else if(cost>o.cost)
            return 1;
        else
            return -1;
    }
}
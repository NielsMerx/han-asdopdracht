package graph;

import datatypes.*;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;


/**
 * Geen inspiratie gekregen van Weiss, want van zijn code/uitleg snap ik bijzonder weinig.
 * Ik gebruik wel hashmap. In de opdracht staat dat je eigen data structuren moet gebruiken, maar Charlotte vroeg in les of we echt alle datastruceren zelf moeten schrijven
 * Het antwoord daarop was dat het niet persé hoefde. Voor GenericArraylist en linkedlist heb ik wel mijn eigen implementatie.
 * Mocht hashmap ook niet mogen, dan kan ik dat uiteraard op een later moment zelf maken en inleveren
 */
public class Graph {

    private GenericArrayListsHAN<NielsVertex> listOfVertexes = new GenericArrayListsHAN<NielsVertex>();

    /**
     * Bereken het korste pad van een graaf zonder rekening te houden met kosten. Dus een ongerichte graaf.
     * We doen dit op basis van BFS (breath first search)
     * Deze video heb ik gebruikt voor uitleg over het algoritme. https://www.youtube.com/watch?v=0GGQXtiCLYs
     * Vervolgens heb ik dit geschreven en aangepast zodat het 't korste pad vindt.
     * @param startVertexName
     * @param vertexToFindName
     * @return
     */
    public int unweightShortestPath(String startVertexName, String vertexToFindName) throws Exception {
        if (!existsInArrayList(startVertexName))
            throw new Exception("Given starting vertex not found");
        if (!existsInArrayList(vertexToFindName))
            throw new Exception("Given vertexToFind  not found");

        // De vertex die we zoeken is dezelfde als waarmee we beginnen. Dus optimalisatie doorgevoerd, zodat niet de rest van de code nodig is.
        if(startVertexName == vertexToFindName)
            return 0;

        NielsVertex startVertex = getVertexOfListBasedOnName(startVertexName);

        HANqueue<NielsVertex> que = new HANqueue<NielsVertex>();
        GenericArrayListsHAN<String> listOfVistedVertex = new GenericArrayListsHAN<String>();
        que.enqueue(startVertex);
        listOfVistedVertex.add(startVertex.getName());

        Map<String, Integer> distances = new HashMap<>();
        distances.put(startVertex.getName(), 0);


        while(!que.isEmpty())
        {
            // De huidige vertex waarvan we gaan kijken met welke die connected is
            NielsVertex currentVertex = que.dequeue();
            for(int i = 0; i < currentVertex.edges.getSize(); i++)
            {
                // Alleen toevoegen als we 'm nog niet hebben bekeken
                if(!listOfVistedVertex.hasValue(currentVertex.edges.get(i).getVertex().getName()))
                {
                    distances.put(currentVertex.edges.get(i).getVertex().getName(), distances.get(currentVertex.getName()) + 1);

                    listOfVistedVertex.add(currentVertex.edges.get(i).getVertex().getName());
                    que.enqueue(getVertexOfListBasedOnName(currentVertex.edges.get(i).getVertex().getName()));

                }
            }
        }


        /*
        Te gebruiken voor debuggen. Dit laat alle afstanden zien.
        System.out.println(distances.keySet());
        System.out.println(distances.values());
        */

        return distances.get(vertexToFindName);
    }


    /**
     * Zelf geschreven. Uitleg over Dijkstra in deze video: https://www.youtube.com/watch?v=pVfj6mxhdMw
     * @param startVertexName
     * @param vertexToFindName
     * @return
     * @throws Exception
     */
    public int dijkstra(String startVertexName, String vertexToFindName) throws Exception
    {
        if (!existsInArrayList(startVertexName))
            throw new Exception("Given starting vertex not found");
        if (!existsInArrayList(vertexToFindName))
            throw new Exception("Given vertexToFind  not found");

        // De vertex die we zoeken is dezelfde als waarmee we beginnen. Dus optimalisatie doorgevoerd, zodat niet de rest van de code nodig is.
        if(startVertexName == vertexToFindName)
            return 0;

        NielsVertex startVertex = getVertexOfListBasedOnName(startVertexName);

        // Lijst met alle vertexes die we bekeken hebben
        GenericArrayListsHAN<String> listOfVistedVertex = new GenericArrayListsHAN<String>();
        // De afstand van elke vertex tot de startVertex
        Map<String, Integer> vertexDistance = new HashMap<>();
        // Juiste begin afstanden zetten
        for(int i = 0; i < this.listOfVertexes.length(); i++)
        {
            if(this.listOfVertexes.get(i).getName() == startVertex.getName())
                vertexDistance.put(this.listOfVertexes.get(i).getName(), 0);
            else
                vertexDistance.put(this.listOfVertexes.get(i).getName(), Integer.MAX_VALUE);
        }

        // De vorige vertex
        Map<String, String> previousVertex = new HashMap<>();

        PriorityQueue<NielsVertex> que = new PriorityQueue<NielsVertex>();
        que.add(startVertex);

        while(!que.isEmpty())
        {
            // De huidige vertex waarvan we gaan kijken met welke die connected is
            NielsVertex currentVertex = que.poll();
            for(int i = 0; i < currentVertex.edges.getSize(); i++)
            {
                NielsEdge currentEdge = currentVertex.edges.get(i);

                // Checken of de vertex wel bestaat in de lijst met bestaande vertexen
                // Dit voorkomt dat er een edge kan zijn die 'n vertex toegvoegd die niet bestaat.
                if(!vertexDistance.containsKey(currentEdge.getVertex().getName()))
                    throw new Exception("The current edge has a vertex connected that doesn't exist in this.listOfVertexes. Name of the vertex: "+ currentEdge.getVertex().getName());

                // Alleen onbezochte buren bekijken
                if(!listOfVistedVertex.hasValue(currentEdge.getVertex().getName()))
                {
                    int currnetDistance = currentEdge.getCost();
                    int currentFastestDistance = vertexDistance.get(currentEdge.getVertex().getName());
                    int costPreviousEdge;

                    // Kijk naar de cost van de vorige edge. Voor de eerste vertex is die er niet, dus 0
                    costPreviousEdge = vertexDistance.get(currentVertex.getName());
                    currnetDistance += costPreviousEdge;


                    // Kijken of de route sneller is
                    if(currentFastestDistance == Integer.MAX_VALUE || currnetDistance < currentFastestDistance)
                    {
                        vertexDistance.put(currentEdge.getVertex().getName(), currnetDistance);
                        previousVertex.put(currentEdge.getVertex().getName(), currentVertex.getName());
                    }

                    que.add(getVertexOfListBasedOnName(currentVertex.edges.get(i).getVertex().getName()));
                }
            }
            listOfVistedVertex.add(currentVertex.getName());
        }


       // Bedoeld voor debugging
        /*
        System.out.println(vertexDistance.keySet());
        System.out.println(vertexDistance.values());
        System.out.println(previousVertex.keySet());
        System.out.println(previousVertex.values());
        */

        return vertexDistance.get(vertexToFindName);
    }

    // Methode geeft het pad terug ipv de kosten
    public String dijkstraString(String startVertexName, String vertexToFindName) throws Exception
    {
        if (!existsInArrayList(startVertexName))
            throw new Exception("Given starting vertex not found");
        if (!existsInArrayList(vertexToFindName))
            throw new Exception("Given vertexToFind  not found");

        // De vertex die we zoeken is dezelfde als waarmee we beginnen. Dus optimalisatie doorgevoerd, zodat niet de rest van de code nodig is.
        if(startVertexName == vertexToFindName)
            return startVertexName;

        NielsVertex startVertex = getVertexOfListBasedOnName(startVertexName);

        // Lijst met alle vertexes die we bekeken hebben
        GenericArrayListsHAN<String> listOfVistedVertex = new GenericArrayListsHAN<String>();
        // De afstand van elke vertex tot de startVertex
        Map<String, Integer> vertexDistance = new HashMap<>();
        // Juiste begin afstanden zetten
        for(int i = 0; i < this.listOfVertexes.length(); i++)
        {
            if(this.listOfVertexes.get(i).getName() == startVertex.getName())
                vertexDistance.put(this.listOfVertexes.get(i).getName(), 0);
            else
                vertexDistance.put(this.listOfVertexes.get(i).getName(), Integer.MAX_VALUE);
        }

        // De vorige vertex
        Map<String, String> previousVertex = new HashMap<>();

        PriorityQueue<NielsVertex> que = new PriorityQueue<NielsVertex>();
        que.add(startVertex);

        while(!que.isEmpty())
        {
            // De huidige vertex waarvan we gaan kijken met welke die connected is
            NielsVertex currentVertex = que.poll();
            for(int i = 0; i < currentVertex.edges.getSize(); i++)
            {
                NielsEdge currentEdge = currentVertex.edges.get(i);

                // Checken of de vertex wel bestaat in de lijst met bestaande vertexen
                // Dit voorkomt dat er een edge kan zijn die 'n vertex toegvoegd die niet bestaat.
                if(!vertexDistance.containsKey(currentEdge.getVertex().getName()))
                    throw new Exception("The current edge has a vertex connected that doesn't exist in this.listOfVertexes. Name of the vertex: "+ currentEdge.getVertex().getName());

                // Alleen onbezochte buren bekijken
                if(!listOfVistedVertex.hasValue(currentEdge.getVertex().getName()))
                {
                    int currnetDistance = currentEdge.getCost();
                    int currentFastestDistance = vertexDistance.get(currentEdge.getVertex().getName());
                    int costPreviousEdge;

                    // Kijk naar de cost van de vorige edge. Voor de eerste vertex is die er niet, dus 0
                    costPreviousEdge = vertexDistance.get(currentVertex.getName());
                    currnetDistance += costPreviousEdge;


                    // Kijken of de route sneller is
                    if(currentFastestDistance == Integer.MAX_VALUE || currnetDistance < currentFastestDistance)
                    {
                        vertexDistance.put(currentEdge.getVertex().getName(), currnetDistance);
                        previousVertex.put(currentEdge.getVertex().getName(), currentVertex.getName());
                    }

                    que.add(getVertexOfListBasedOnName(currentVertex.edges.get(i).getVertex().getName()));
                }
            }
            listOfVistedVertex.add(currentVertex.getName());
        }


        // Bedoeld voor debugging
        /*
        System.out.println(vertexDistance.keySet());
        System.out.println(vertexDistance.values());
        System.out.println(previousVertex.keySet());
        System.out.println(previousVertex.values());
        */

        String routeVertex = vertexToFindName;
        GenericArrayListsHAN<String> routeReversalArray = new GenericArrayListsHAN<String>();
        while(true)
        {
            if(previousVertex.containsKey(routeVertex))
            {
                routeReversalArray.add(routeVertex + "->");
                routeVertex = previousVertex.get(routeVertex);
            }
            else
            {
                routeReversalArray.add(startVertexName + "->");
                break;
            }
        }


        String route = "";
        for(int i = routeReversalArray.length() -1; i>=0; i--)
        {
            route += routeReversalArray.get(i);
        }

        return route;
    }



    public void showGraphAsAdjacencyList()
    {
        for(int i = 0; i < this.listOfVertexes.length(); i++)
        {
            NielsVertex currentVertex = this.listOfVertexes.get(i);
            String adjecenyListString = currentVertex.getName() + " -> ";
            // Loop door alle edges heen en print dat naar het scherm
            if(currentVertex.isConnected())
            {
                for(int j =0; j < currentVertex.edges.getSize(); j++)
                {
                    adjecenyListString += currentVertex.edges.get(j).getVertex().getName() + "(" + currentVertex.edges.get(j).getCost() + ") ->";
                }
            }

            System.out.println(adjecenyListString);
                
        }
        
        System.out.println("TOELICHTING: Elke eerste vertex kan naar alle door de pijl aangegeven vertexen");

    }


    /**
     * add vertex to list of vertxes.
     * @param vertex
     * @throws Exception
     */
    public void addToArrayList(NielsVertex vertex) throws Exception {
        if(this.existsInArrayList(vertex))
            throw new Exception("Given vertex " + vertex.getName() + " already exists");

        this.listOfVertexes.add(vertex);
    }

    /**
     * Controleert of de gegeven vertex niet al voorkomt in de lijst met vertexes.
     * @param vertexName
     * @return boolean.
     */
    private boolean existsInArrayList(String vertexName)
    {
        for(int i = 0; i < this.listOfVertexes.length(); i++)
        {
            NielsVertex currentVertex = this.listOfVertexes.get(i);
            if(currentVertex.getName() == vertexName)
                return true;
        }

        return false;
    }

    /**
     * Controleert of de gegeven vertex niet al voorkomt in de lijst met vertexes.
     * @param vertex
     * @return boolean.
     */
    private boolean existsInArrayList(NielsVertex vertex)
    {
        String vertexName = vertex.getName();
        for(int i = 0; i < this.listOfVertexes.length(); i++)
        {
            NielsVertex currentVertex = this.listOfVertexes.get(i);
            if(currentVertex.getName() == vertexName)
                return true;
        }

        return false;
    }

    private NielsVertex getVertexOfListBasedOnName(String name) throws Exception {
        for(int i = 0; i < this.listOfVertexes.length(); i++)
        {
            NielsVertex currentVertex = this.listOfVertexes.get(i);
            if(currentVertex.getName() == name)
                return currentVertex;
        }

        throw new Exception("Given vertex not found with name: " + name);
    }
}


package graph;

public class main {

    public static void main(String[] args) {

        testGraph();

    }

    private static void testGraph() {
        try {
            Graph f = new Graph();
            NielsVertex v0 = new NielsVertex("v0");
            v0.add(new NielsEdge(new NielsVertex("v1")), 2);
            v0.add(new NielsEdge(new NielsVertex("v3")), 1);

            NielsVertex v1 = new NielsVertex("v1");
            v1.add(new NielsEdge(new NielsVertex("v3")), 3);
            v1.add(new NielsEdge(new NielsVertex("v4")), 10);

            NielsVertex v2 = new NielsVertex("v2");
            v2.add(new NielsEdge(new NielsVertex("v0")), 4);
            v2.add(new NielsEdge(new NielsVertex("v5")), 5);

            NielsVertex v3 = new NielsVertex("v3");
            v3.add(new NielsEdge(new NielsVertex("v2")), 2);
            v3.add(new NielsEdge(new NielsVertex("v5")), 8);
            v3.add(new NielsEdge(new NielsVertex("v6")), 4);
            v3.add(new NielsEdge(new NielsVertex("v4")), 2);

            NielsVertex v4 = new NielsVertex("v4");
            v4.add(new NielsEdge(new NielsVertex("v6")), 6);

            NielsVertex v5 = new NielsVertex("v5");

            NielsVertex v6 = new NielsVertex("v6");
            v6.add(new NielsEdge(new NielsVertex("v5")), 1);

            f.addToArrayList(v0);
            f.addToArrayList(v1);
            f.addToArrayList(v2);
            f.addToArrayList(v3);
            f.addToArrayList(v4);
            f.addToArrayList(v5);
            f.addToArrayList(v6);


            f.showGraphAsAdjacencyList();
            System.out.println("-----------");
            //  System.out.println("De korste routen tussen de aangegeven vertexes zonder kosten (unweight) is: " + f.unweightShortestPath("v0","v2"));
            System.out.println("De korste routen tussen de aangegeven vertexes met kosten (weight) is: " + f.dijkstra("v0", "v5"));
            System.out.println("De korste routen is: " + f.dijkstraString("v0", "v5"));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}

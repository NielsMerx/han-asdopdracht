package datatypes;

public class HANqueue<AnyType> {
    private GenericArrayListsHAN theArray;

    public HANqueue()
    {
        this.theArray = new GenericArrayListsHAN<AnyType>();
    }

    public void enqueue(AnyType insertValue)
    {
        this.theArray.add(insertValue);
    }

    public AnyType dequeue()
    {
        AnyType value = (AnyType) this.theArray.get(0);

        if(theArray.get(1) != null)
        {
            for(int i = 0; i < this.theArray.length(); i++)
            {
                this.theArray.set(i, this.theArray.get(i + 1));
            }
        }
        else // We vragen laatste item op, dus daana kunnen we de array leeggooien.
            this.theArray =  new GenericArrayListsHAN<AnyType>();


        return value;
    }

    public AnyType getFront()
    {
        return (AnyType) this.theArray.get(0);
    }

    public boolean isEmpty()
    {
        return this.theArray.get(0) == null;
    }
}

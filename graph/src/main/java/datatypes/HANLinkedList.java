package datatypes;

public class HANLinkedList<AnyType> {

    private HANLinkedListNode firstNode;

    public void addFirst(AnyType value)
    {
        this.firstNode = this.createNode(value);
    }

    public void removeFirst()
    {
        if(this.firstNode.getNode() != null)
            this.firstNode = this.firstNode.getNode();
        else
            this.firstNode = null;
    }

    public AnyType get(int index)
    {
        HANLinkedListNode tmp = this.firstNode;
        if(index == 0)
            return (AnyType) this.firstNode.getValue();

        for(int i=0; i<=index; i++)
        {
            if(index == i)
                return (AnyType) tmp.getValue();
            else
                tmp = tmp.getNode();

        }
        return null;
    }

    public int getSize()
    {
        int i = 1;
        HANLinkedListNode tmp = this.firstNode;
        if(tmp != null)
        {
            while(tmp.getNode() != null)
            {
                tmp = tmp.getNode();
                i++;
            }
        }
        else
            return 0;

        return i;
    }

    public void insert(int index, AnyType value)
    {
        HANLinkedListNode tmp = this.firstNode;
        for(int i=0; i<=index; i++)
        {
            if(index- 1 == i)
            {
                if(tmp.node == null)
                {
                    tmp.node =  createNode(value);
                    return;
                }
                else
                    tmp.setValue(value);
            }
            else
                tmp = tmp.getNode();
        }
    }

    public void add(AnyType value)
    {
        HANLinkedListNode tmp = this.firstNode;

        if(tmp == null)
            this.addFirst(value);
        else
        {
            while(true)
            {
                if(tmp.getNode() == null)
                {
                    tmp.node = createNode(value);
                    return;
                }
                tmp = tmp.getNode();
            }
        }
    }

    public void delete(int index)
    {
        HANLinkedListNode tmp = this.firstNode;
        int i =0;

        if(index == 0)
            this.firstNode = tmp.getNode();
        else
        {
            while(tmp != null)
            {
                if(i == (index -1))
                {
                    tmp.node = tmp.getNode().getNode();
                    return;
                }
                else
                    tmp = tmp.getNode();
                i++;
            }
        }
    }

    public void printAll()
    {
        HANLinkedListNode tmp = this.firstNode;
        while(tmp != null)
        {
            System.out.println(tmp.getValue());
            tmp = tmp.getNode();
        }
    }

    private HANLinkedListNode createNode(AnyType value)
    {
        HANLinkedListNode<AnyType> node = new HANLinkedListNode<AnyType>();
        node.setValue(value);
        return node;
    }



}

class HANLinkedListNode<AnyType>
{
    private AnyType value;
    public HANLinkedListNode node;

    public AnyType getValue() {
        return value;
    }

    public void setValue(AnyType value) {
        this.value = value;
    }

    public HANLinkedListNode getNode() {
        return node;
    }

    public void setNode(HANLinkedListNode node) {
        this.node = node;
    }
}
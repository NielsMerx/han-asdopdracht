package datatypes;

public class GenericArrayListsHAN<AnyType> {
    private AnyType theArray[];
    private int arraySize =  5;
    public GenericArrayListsHAN()
    {
        AnyType[] theArray = (AnyType[]) new Object[arraySize];
        this.theArray = theArray;
    }

    public void add(AnyType value)
    {
        for(int i = 0; i < this.theArray.length; i++)
        {
            if(this.theArray[i] == null)
            {
                set(i,value);
                return;
            }
        }
    }

    public AnyType get(int index)
    {
        if(this.theArray[index] != null)
            return theArray[index];
        else
            return null;
    }

    public void set(int index, AnyType value)
    {
        this.fullArrayCheck();
        theArray[index] = value;
    }

    public int length()
    {
        int length = 0;
        for(int i = 0; i <= this.theArray.length; i++)
        {
            if(this.theArray[i] == null)
                return length;
            else
              length++;
        }

        return length;
    }

    public boolean hasValue(AnyType value)
    {
        for(int i = 0; i < this.theArray.length; i++)
        {
            if(this.theArray[i] == value)
            {
                return true;
            }
        }

        return false;
    }

    public void printArray()
    {
        for(int i = 0; i < this.theArray.length; i++)
            System.out.println("on spot:" + i + " has the value: " + this.theArray[i]);
    }

    private void fullArrayCheck()
    {
        // Does null check instead of max size, since everything is filled with null so filled.
        if (this.theArray[arraySize - 2] != null ) {
            AnyType[] old = this.theArray;
            this.theArray = (AnyType[]) new Object[arraySize * 2 + 1];
            for (int i = 0; i < arraySize; i++) {
                this.theArray[i] = old[i];
            }
            arraySize = arraySize * 2 + 1;
        }

    }
}

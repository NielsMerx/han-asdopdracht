import graph.*;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class vertexTest {
    @Test
    public void testIfWeCanGiveANameToAVertex() throws Exception {
        NielsVertex vertex = new NielsVertex("Niels");
        assertEquals("Niels", vertex.getName());
    }

    @Test
    public void testIfWeCanAddAdgesToAVertex() throws Exception {
        NielsVertex vertex = new NielsVertex("Niels");
        vertex.add(new NielsEdge(new NielsVertex("v1")),2);

        assertTrue(vertex.isConnected());
    }

    @Test
    public void testIfWeCanGiveCostToEdge() throws Exception {
        NielsVertex vertex = new NielsVertex("Niels");
        vertex.add(new NielsEdge(new NielsVertex("v1")),2);

        assertEquals(2, vertex.edges.get(0).getCost());
    }

    @Test
    public void testIfConnectedReturnsFalseOnNotConnectedVertex() throws Exception {
        NielsVertex vertex = new NielsVertex("Niels");

        assertFalse(vertex.isConnected());
    }
}

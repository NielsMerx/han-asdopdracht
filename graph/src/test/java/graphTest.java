import graph.Graph;
import graph.NielsEdge;
import graph.NielsVertex;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class graphTest {

    private Graph graph;
    @Before
    public void setUp() throws Exception {
        Graph f = new Graph();
        NielsVertex v0 = new NielsVertex("v0");
        v0.add(new NielsEdge(new NielsVertex("v1")),2);
        v0.add(new NielsEdge(new NielsVertex("v3")),1);

        NielsVertex v1 = new NielsVertex("v1");
        v1.add(new NielsEdge(new NielsVertex("v3")),3);
        v1.add(new NielsEdge(new NielsVertex("v4")),10);

        NielsVertex v2 = new NielsVertex("v2");
        v2.add(new NielsEdge(new NielsVertex("v0")),4);
        v2.add(new NielsEdge(new NielsVertex("v5")),5);

        NielsVertex v3 = new NielsVertex("v3");
        v3.add(new NielsEdge(new NielsVertex("v2")),2);
        v3.add(new NielsEdge(new NielsVertex("v5")),8);
        v3.add(new NielsEdge(new NielsVertex("v6")),4);
        v3.add(new NielsEdge(new NielsVertex("v4")),2);

        NielsVertex v4 = new NielsVertex("v4");
        v4.add(new NielsEdge(new NielsVertex("v6")),6);

        NielsVertex v5 = new NielsVertex("v5");

        NielsVertex v6 = new NielsVertex("v6");
        v6.add(new NielsEdge(new NielsVertex("v5")),1);

        f.addToArrayList(v0);
        f.addToArrayList(v1);
        f.addToArrayList(v2);
        f.addToArrayList(v3);
        f.addToArrayList(v4);
        f.addToArrayList(v5);
        f.addToArrayList(v6);

        this.graph = f;
    }

    @Test
    public void testIfShortestRouteToV0ToV0is0ByBFS() throws Exception {
        assertEquals(0, this.graph.unweightShortestPath("v0","v0"));
    }
    @Test
    public void testIfShortestRouteToV0ToV0is0ByDijkstra() throws Exception {
        assertEquals(0, this.graph.dijkstra("v0","v0"));
    }

    @Test
    public void testIfBFSShortestRouteForv0ToV1Is1() throws Exception {
        assertEquals(1, this.graph.unweightShortestPath("v0","v1"));
    }

    @Test
    public void testIfDijkstrahortestRouteForv0ToV5Is6() throws Exception {
        assertEquals(6, this.graph.dijkstra("v0","v5"));
    }
    @Test
    public void testIBFSRouteForv0ToV5Is2() throws Exception {
        assertEquals(2, this.graph.unweightShortestPath("v0","v5"));
    }

    @Test
    public void testIfWeCanAddNewVertexToGraph() throws Exception {
        NielsVertex v7 = new NielsVertex("v7");
        v7.add(new NielsEdge(new NielsVertex("v1")),35);

        this.graph.addToArrayList(v7);
    }

    @Test(expected = Exception.class)
    public void testIfWeCanAddExistingVertexToGraph() throws Exception {
        NielsVertex v0 = new NielsVertex("v0");
        v0.add(new NielsEdge(new NielsVertex("v1")),2);
        v0.add(new NielsEdge(new NielsVertex("v3")),1);

        this.graph.addToArrayList(v0);
    }
}

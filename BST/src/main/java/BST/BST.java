package BST;

import java.util.LinkedList;
import java.util.Queue;

public class BST {

    private BinaryNielsNode root = null;

    public BinaryNielsNode find(Integer findValue) throws Exception {
        BinaryNielsNode result = this.find(findValue, this.root);
        if (result == null)
            throw new Exception("Gegeven value " + findValue + " bevind zich niet in de boom");
        else
            return result;
    }


    public void remove(Integer value)
    {
        this.remove(value, this.root);
    }

    public BinaryNielsNode findMin()
    {
        return findMin(this.root);
    }

    public BinaryNielsNode findMax()
    {
        return findMax(this.root);
    }


    public void insert(Integer value) throws Exception {
        if(this.root == null)
            this.root = new BinaryNielsNode(value, null, null);
        else
            this.insert(value, this.root);
    }


    public void printLevelOrder()
    {
        this.printLevelOrder(this.root);
    }


    /*
        Niet nodig voor de opdrachten, maar handig voor debug.
        Current, Left, Right
         Output validatie gechecked: https://yongdanielliang.github.io/animation/web/BST.html
     */
    public void preOrder()
    {
        this.preOrder(this.root);
    }

    /*
       Niet nodig voor de opdrachten, maar handig voor debug.
        Left, Right, Current
        Output validatie gechecked: https://yongdanielliang.github.io/animation/web/BST.html
     */
    public void postOrder()
    {
        postOrder(this.root);
    }


    /**
     *    Niet nodig voor de opdrachten, maar handig voor debug.
     *    left, current, right
     *    Voor nu print die het gewoon naar het schem.
     *     Output validatie gechecked: https://yongdanielliang.github.io/animation/web/BST.html
     */
    public void inOrder()
    {
        this.inOrder(this.root);
    }

    public BinaryNielsNode findParrentOfNode(Integer value)
    {
        return this.findParrentOfNode(value, this.root);
    }

    private BinaryNielsNode findParrentOfNode(Integer findValue, BinaryNielsNode node)
    {
         if( (node.left != null && node.left.value == findValue) || (node.right != null && node.right.value == findValue))
            return node;

        if(findValue.compareTo(node.value) < 0)
            return findParrentOfNode(findValue, node.left);
        else if(findValue.compareTo(node.value) > 0)
            return findParrentOfNode(findValue, node.right);

        // Root note dus return null, want geen parrent.
        return null;

    }

    // Video voor de informatie: https://app.pluralsight.com/course-player?course=ads-part1&author=robert-horvick&name=ads-binarytree&clip=0&mode=live
    // CASE 1 REMOVED NODE HAS NO RIGHT CHILD: lEFT CHILD REPLACES REMOVED.
    // CASE 2 REMOVED RIGHT NODE CHILD HAS NO LEFT CHILD: RIGHT CHILD REPLACED REMOVED.(node child, heeft een rechterchild, maar dat rechter child heeft dan geen linker child)
    // CASE 3 REMOVED RIGHT NODE CHILD HAS LEFT CHILD: RIGHT CHILDS MOST LEFT CHILD REPLACES REMOVED NOTE. (node child heeft rechterchild en dat rechterchild heeft een leftchild. De meest linkse child wordt dan de verwijderde node).
    private BinaryNielsNode remove(Integer removeValue, BinaryNielsNode node)
    {
        if(node == null)
            return node;

        // Omdat het altijd ints zijn hoeft het natuurlijk niet met compareTo. Maar als je er anytype(comparable) van maakt, is dit wel een mooiere oplossing
        if(removeValue.compareTo(node.value) < 0)
            return remove(removeValue, node.left);
        else if(removeValue.compareTo(node.value) > 0)
            return remove(removeValue, node.right);
        else if(node.right == null) // case 1
        {
            BinaryNielsNode parrent = this.findParrentOfNode(removeValue);
            if(parrent == null)
                this.root = node.left;
            else
            {
               if(removeValue.compareTo(parrent.value) <0)
                   parrent.left = node.left;
               else if(removeValue.compareTo(parrent.value) >0)
                   parrent.right = node.left;
            }
        }
        else if(node.right.left == null) // case 2
        {
            node.right.left = node.left;

            BinaryNielsNode parrent = this.findParrentOfNode(removeValue);
            if(parrent == null)
                this.root = node.left;
            else
            {
                if(removeValue.compareTo(parrent.value) <0)
                    parrent.left = node.right;
                else if(removeValue.compareTo(parrent.value) >0)
                    parrent.right = node.right;
            }
        }
        else // case 3
        {
            // Dit wordt de vervanger voor de node die we verwijderen
            BinaryNielsNode leftMostNode = findMin(node.right.left);
            BinaryNielsNode leftMostNodeParrent = findParrentOfNode(leftMostNode.value);

            // Parent linker subtree wordt leftmost rechter subtree.
            leftMostNodeParrent.left = leftMostNode.right;

            leftMostNode.left = node.left;
            leftMostNode.right = node.right;

            BinaryNielsNode parrent = this.findParrentOfNode(removeValue);
            if(parrent == null)
                this.root = leftMostNode;
            else
            {
                if(removeValue.compareTo(parrent.value) <0)
                    parrent.left = leftMostNode;
                else if(removeValue.compareTo(parrent.value) >0)
                    parrent.right = leftMostNode;
            }


        }
        return node;
    }

    private void postOrder(BinaryNielsNode node)
    {
        if( node != null )
        {
            postOrder( node.left );
            postOrder(node.right );
            System.out.print( node.value + " ");
        }
    }


    private void preOrder(BinaryNielsNode node)
    {
        if( node != null )
        {
            System.out.print( node.value + " ");
            preOrder( node.left );
            preOrder(node.right );
        }
    }



    private void inOrder(BinaryNielsNode node)
    {
        if( node != null )
        {
            inOrder( node.left );
            System.out.print( node.value + " ");
            inOrder(node.right );
        }
    }




    private BinaryNielsNode insert(Integer value, BinaryNielsNode node) throws Exception {
        if( node == null )
            return new BinaryNielsNode(value, null, null );

        int compareResult = value.compareTo(node.value);

        if( compareResult < 0 )
            node.left = insert(value , node.left );
        else if( compareResult > 0 )
            node.right = insert( value, node.right );
        else
            throw new Exception("Geen dubbele waarden toegestaan. " + node.value + " bevind zich al in de boom");
        return node;
    }

    private BinaryNielsNode findMin(BinaryNielsNode node)
    {
        if(node.left == null)
            return node;

        return findMin(node.left);
    }

    private BinaryNielsNode findMax(BinaryNielsNode node)
    {
        if(node.right == null)
            return node;

        return findMax(node.right);
    }

    private BinaryNielsNode find(Integer findValue, BinaryNielsNode node)
    {
        if(node == null)
            return null;

        if(findValue.compareTo(node.value) < 0)
            return find(findValue, node.left);
        else if(findValue.compareTo(node.value) > 0)
            return find(findValue, node.right);
        else
            return node;
    }
    /**
     * Methode gekopiërd van deze website: https://www.geeksforgeeks.org/print-level-order-traversal-line-line/
     * Ik wilde graag weten of mijn boom klopt in volgorde vandaar dat ik deze methode nodig heb.
     * @param root
     */
    private void printLevelOrder(BinaryNielsNode root)
    {
        // Base Case
        if(root == null)
            return;

        // Create an empty queue for level order tarversal
        Queue<BinaryNielsNode> q =new LinkedList<BinaryNielsNode>();

        // Enqueue Root and initialize height
        q.add(root);
        while(true)
        {

            // nodeCount (queue size) indicates number of nodes
            // at current level.
            int nodeCount = q.size();
            if(nodeCount == 0)
                break;

            // Dequeue all nodes of current level and Enqueue all
            // nodes of next level
            while(nodeCount > 0)
            {
                BinaryNielsNode node = q.peek();
                System.out.print(node.value + " ");
                q.remove();
                if(node.left != null)
                    q.add(node.left);
                if(node.right != null)
                    q.add(node.right);
                nodeCount--;
            }
            System.out.println();
        }
    }

}
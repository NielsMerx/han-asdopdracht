package BST;
public class main {

    public static void main( String [ ] args )
    {
        // test
        testBST();

    }

    private static void testBST()
    {
         try {
         BST bst = new BST();
         bst.insert(10);
         bst.insert(5);
         bst.insert(115);
         bst.insert(117);
         bst.insert(18);
         bst.insert(37);
         bst.insert(11);
         bst.insert(12);
         bst.insert(99);
         bst.insert(116);
         bst.printLevelOrder();
         }catch (Exception e)
         {
         System.out.println(e.getMessage());
         }
    }

}

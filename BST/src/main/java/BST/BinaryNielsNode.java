package BST;

public class BinaryNielsNode
{
    public Integer value;
    public BinaryNielsNode left;
    public BinaryNielsNode right;

    public BinaryNielsNode(Integer value, BinaryNielsNode left, BinaryNielsNode right)
    {
        this.value = value;
        this.left = left;
        this.right = right;
    }
}
import BST.BinaryNielsNode;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
public class BinaryNielsNodeTest {

    @Test
    public void testIfNodeCanBeMade() throws Exception
    {
        BinaryNielsNode node = new BinaryNielsNode(10, null,null);
        assertEquals(10, (int) node.value);
    }

    @Test
    public void testIfNodeCanHaveLeftNode() throws Exception
    {
        BinaryNielsNode node = new BinaryNielsNode(10, null,null);
        node.left = new BinaryNielsNode(5,null,null);
        assertEquals(5, (int) node.left.value);
    }

    @Test
    public void testIfNodeCanHaveRightNode() throws Exception
    {
        BinaryNielsNode node = new BinaryNielsNode(10, null,null);
        node.right = new BinaryNielsNode(5,null,null);
        assertEquals(5, (int) node.right.value);
    }
}

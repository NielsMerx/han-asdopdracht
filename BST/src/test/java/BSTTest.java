import BST.*;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class BSTTest {

    private BST bst = new BST();
    @Before
    public void setUp() throws Exception {
        bst.insert(10);
        bst.insert(5);
        bst.insert(115);
        bst.insert(117);
        bst.insert(18);
        bst.insert(37);
        bst.insert(11);
        bst.insert(12);
        bst.insert(99);
        bst.insert(116);
    }

    @Test
    public void testGetMinValue() throws Exception
    {
        assertEquals(5, (int) this.bst.findMin().value);
    }

    @Test
    public void testGetMinValueAfterDelete() throws Exception
    {
        bst.remove(5);
        assertEquals(10, (int) this.bst.findMin().value);
    }

    @Test
    public void testMaxValue() throws Exception
    {
        assertEquals(117, (int) this.bst.findMax().value);
    }

    @Test
    public void testMaxValueAfterDelete() throws Exception
    {
        bst.remove(117);
        assertEquals(116, (int) this.bst.findMax().value);
    }

    @Test
    public void testAdd() throws Exception
    {
        bst.insert(1);
        assertEquals(1, (int) this.bst.findMin().value);
    }

    @Test(expected = Exception.class)
    public void testAddingExistingValue() throws Exception
    {
        // 5 bestaat al in de boom, dus we zouden exception moeten krijgen
        bst.insert(5);
    }

    @Test
    public void testIf5IsleftOf10() throws Exception
    {
        BinaryNielsNode node = bst.find(10);
        assertEquals(5, (int) node.left.value);
    }

    @Test
    public void testIfParrentCanBeFound() throws Exception
    {
        BinaryNielsNode node = bst.findParrentOfNode(5);
        assertEquals(10, (int) node.value);
    }

    @Test
    public void testIfParrentCanNotBeFound() throws Exception
    {
        BinaryNielsNode node = bst.findParrentOfNode(10);
        assertNull(node);
    }


}

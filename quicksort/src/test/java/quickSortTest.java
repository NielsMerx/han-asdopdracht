import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;

import quicksort.*;

import static org.junit.Assert.*;

/**
 * Class tests my quick quickSortNiels
 */
public class quickSortTest {

    @Test
    public void testOutputStrings() throws Exception
    {
        String[] testArrayForJavaSortFuction = {"abc","a","abc1","aaaa","afba","acbc"};
        String[] testArrayForNielsQuiksortFuction = {"abc","a","abc1","aaaa","afba","acbc"};

        Arrays.sort(testArrayForJavaSortFuction);
        quicksort.quickSortNiels(testArrayForNielsQuiksortFuction, false);

        assertTrue(Arrays.equals(testArrayForJavaSortFuction, testArrayForNielsQuiksortFuction));

    }

    @Test
    public void testOutputInteger() throws Exception
    {
        Integer[] testArrayForJavaSortFuction = { 1, 3, 4, 5,18,7,8,2,14,75};
        Integer[] testArrayForNielsQuiksortFuction = { 1, 3, 4, 5,18,7,8,2,14,75};

        Arrays.sort(testArrayForJavaSortFuction);
        quicksort.quickSortNiels(testArrayForNielsQuiksortFuction, false);

        Assert.assertArrayEquals(testArrayForJavaSortFuction,testArrayForNielsQuiksortFuction);
    }

    @Test
    public void testOutputIntegerDecending() throws Exception
    {
        Integer[] testArrayForJavaSortFuction = { 1, 3, 4, 5,18,7,8,2,14,75};
        Integer[] testArrayForNielsQuiksortFuction = { 1, 3, 4, 5,18,7,8,2,14,75};

        Arrays.sort(testArrayForJavaSortFuction, Collections.reverseOrder());
        quicksort.quickSortNiels(testArrayForNielsQuiksortFuction, true);

        Assert.assertArrayEquals(testArrayForJavaSortFuction,testArrayForNielsQuiksortFuction);
    }

    @Test
    public void testOutputIntegerString() throws Exception
    {
        String[] testArrayForJavaSortFuction = {"abc","a","abc1","aaaa","afba","acbc"};
        String[] testArrayForNielsQuiksortFuction = {"abc","a","abc1","aaaa","afba","acbc"};

        Arrays.sort(testArrayForJavaSortFuction, Collections.reverseOrder());
        quicksort.quickSortNiels(testArrayForNielsQuiksortFuction, true);

        Assert.assertArrayEquals(testArrayForJavaSortFuction,testArrayForNielsQuiksortFuction);
    }


    @Test
    public void testOutputStringWeissVsNiels() throws Exception
    {
        String[] testArrayForJavaSortFuction = {"abc","a","abc1","aaaa","afba","acbc"};
        String[] testArrayForNielsQuiksortFuction = {"abc","a","abc1","aaaa","afba","acbc"};

        WeissQuicksort.quicksort(testArrayForJavaSortFuction);
        quicksort.quickSortNiels(testArrayForNielsQuiksortFuction, false);

        Assert.assertArrayEquals(testArrayForJavaSortFuction,testArrayForNielsQuiksortFuction);
    }

    @Test
    public void testOutputIntegerWeissVsNiels() throws Exception
    {
        Integer[] testArrayForJavaSortFuction = { 1, 3, 4, 5,18,7,8,2,14,75};
        Integer[] testArrayForNielsQuiksortFuction = { 1, 3, 4, 5,18,7,8,2,14,75};

        WeissQuicksort.quicksort(testArrayForJavaSortFuction);
        quicksort.quickSortNiels(testArrayForNielsQuiksortFuction, false);

        Assert.assertArrayEquals(testArrayForJavaSortFuction,testArrayForNielsQuiksortFuction);
    }

    @Test
    public void testOutputEmptyArray() throws Exception
    {
        Integer[] testArrayForNielsQuiksortFuction = {};
        quicksort.quickSortNiels(testArrayForNielsQuiksortFuction, false);
        Assert.assertNotNull(testArrayForNielsQuiksortFuction);
    }

    @Test
    public void testIfTwoSortNumbersAreCorrectNotDecending() throws Exception
    {
        Integer[] testArrayForJavaSortFuction = { 1, 3};
        quicksort.quickSortNiels(testArrayForJavaSortFuction, false);

        // Checken of 1 minder is dan 3
        assertTrue(testArrayForJavaSortFuction[0] < testArrayForJavaSortFuction[1]);

    }

    @Test
    public void testIfTwoSortNumbersAreCorrectDecending() throws Exception
    {
        Integer[] testArrayForJavaSortFuction = { 1, 3};
        quicksort.quickSortNiels(testArrayForJavaSortFuction, true);

        // Checken of 1 meer is dan 3
        assertTrue(testArrayForJavaSortFuction[0] > testArrayForJavaSortFuction[1]);
    }

    @Test
    public void testIfOnlyOneNumber() throws Exception
    {
        Integer[] testArrayForJavaSortFuction = { 1};
        quicksort.quickSortNiels(testArrayForJavaSortFuction, false);

       assertSame(1,testArrayForJavaSortFuction[0]);
    }

    @Test
    public void testEqualNumbers() throws Exception
    {
        Integer[] testArrayForJavaSortFuction = { 1, 1, 3};
        quicksort.quickSortNiels(testArrayForJavaSortFuction, false);

        assertSame(testArrayForJavaSortFuction[0], testArrayForJavaSortFuction[1]);
    }

    @Test
    public void testEqualNumbersWithThirdNumberHigher() throws Exception
    {
        Integer[] testArrayForJavaSortFuction = { 1, 1, 3};
        quicksort.quickSortNiels(testArrayForJavaSortFuction, false);

        assertSame(3, testArrayForJavaSortFuction[2]);
    }

    @Test
    public void testEqualNumbersWithThirdNumberHigherDecending() throws Exception
    {
        Integer[] testArrayForJavaSortFuction = { 1, 1, 3};
        quicksort.quickSortNiels(testArrayForJavaSortFuction, true);

        assertSame(1, testArrayForJavaSortFuction[2]);
        assertSame(1, testArrayForJavaSortFuction[1]);
    }

    @Test(expected = Exception.class)
    public void testIfExceptionIsShownWithNulValue() throws Exception
    {
        Integer[] testArrayForJavaSortFuction = {1,2,3,4, null};
        quicksort.quickSortNiels(testArrayForJavaSortFuction, true);

        assertSame(4, testArrayForJavaSortFuction[0]);
    }

}

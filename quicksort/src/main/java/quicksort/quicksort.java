package quicksort;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class quicksort {

    private static <T extends Comparable> void quickSortNiels(T arr[], int low, int high, boolean decendingOrder)
    {
        if (low < high)
        {
            int partionIndex;
            Comparable pivot = arr[high];
            int df = (low-1);

            int tLow = low;
            while(tLow < high)
            {
                if(decendingOrder == true && arr[tLow].compareTo(pivot) > 0)
                {
                    df++;
                    swapPlaces(arr,df,tLow);
                }
                else if(decendingOrder == false && arr[tLow].compareTo(pivot) < 0)
                {
                    df++;
                    swapPlaces(arr,df,tLow);
                }
                tLow++;
            }

            swapPlaces(arr, df+1, high);
            partionIndex = df +1;

            quickSortNiels(arr, low, partionIndex-1, decendingOrder);
            quickSortNiels(arr, partionIndex+1, high, decendingOrder);
        }
    }

    private static <T extends Comparable> void swapPlaces(T arr[], int indexToSwap1, int indexToSwap2)
    {
        T temp = arr[indexToSwap1];
        arr[indexToSwap1] = arr[indexToSwap2];
        arr[indexToSwap2] = temp;
    }

    public static <T extends Comparable> void quickSortNiels(T[] arr, boolean decendingOrder) throws Exception {
        // Als er maar 1 of 0 elementen in de array zitten heeft het geen zin om te sorteren. Bij 1 element is de array natuurlijk al direct gesorteerd.
        if(arr.length == 1 | arr.length == 0)
            return;
        else
        {
            checkForNull(arr);
            quickSortNiels(arr, 0, arr.length-1, decendingOrder);
        }

    }

    private static <T extends Comparable> void checkForNull(T[] arr) throws Exception {
         for(T elm: arr)
         {
             if(elm == null)
                 throw new Exception("Je kan niet sorten met een null waarden, omdat dit niet compearable is. Verwijder de null waarden en probeer het opnieuw");
         }
    }
}

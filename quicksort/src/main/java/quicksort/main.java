package quicksort;

public class main {

    public static void main( String [ ] args ) throws Exception {
       testQuickSort();
    }

    private static void testQuickSort() throws Exception {
        Integer[] intArray1 = { 1, 3, 4, 5,18,7,8,2,14,75};
        quicksort.quickSortNiels(intArray1, false);

        for(Integer elm: intArray1)
        {
            System.out.printf("%s ", elm);
        }
    }
}
